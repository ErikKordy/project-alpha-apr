from django.db import models


# Create your models here.


class Task(models.Model):
    name = models.CharField(max_length=200)
    start_date = models.DateTimeField(default=None)
    due_date = models.DateTimeField(null=False, blank=False, default=None)
    is_completed = models.BooleanField(default=False)
    project = models.ForeignKey(
        "projects.Project",
        related_name="tasks",
        on_delete=models.CASCADE,
    )
    assignee = models.ForeignKey(
        "auth.User",
        null=True,
        related_name="tasks",
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.name
